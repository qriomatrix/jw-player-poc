const
    AWS = require('aws-sdk'),
    s3 = new AWS.S3({signatureVersion: 'v4'});


exports.handler = async (event) => {

    const {bucketName, key} = event;
    console.log("Creating pre sign URL for bucket: %s, key: %s", bucketName, key);

    try {
        // Download
        const url = await  s3.getSignedUrlPromise(
            'getObject',
            {
                Bucket: bucketName,
                Key: key,
                Expires: 6000 });
        
        console.log("Successfully created pre sign URL for bucket: %s, key: %s", bucketName, key);
        return { statusCode: 200, body: { url: url}};
    } 
    catch (e) {
        console.log("Error:", e);
        return  { statusCode: 500, body: { err: e}};
    }
};